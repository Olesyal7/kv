#pragma once
#include "Graph.h"
#include "DSU.h"

using namespace std;

class MST {
public:
	MST(Graph& _graph);
	const vector<vector<double>>& get_matrix();
	void build();
	double aprox();
private:
	vector<vector<double>> matrix;
	vector<vector<double>> Tree;
	vector<int> order;
	vector<int> color;
	void DFS(int i, int& count);
};