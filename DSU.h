#pragma once
#include <vector>

using namespace std;

class DSU {
public:
	void buffer(int size);
	int get_set(int ex);
	void unions(int ex1, int ex2);
private:
	vector<int> root;
	vector<int> rank;
};