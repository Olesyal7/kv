#pragma once
#include <vector>

using namespace std;

class Graph {
public:
	Graph(int size);
	void add_edge(int from, int to, double weight);
	const vector<vector<double>>& get_matrix();
private:
	int ver;
	vector<vector<double>> matrix;
};