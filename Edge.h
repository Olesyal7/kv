#pragma once

struct Edge {
	Edge(int fromc, int toc, double weight);
	int from;
	int to;
	double weight;
};