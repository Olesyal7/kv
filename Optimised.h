#pragma once
#include "Graph.h"
#include <vector>

using namespace std;

class Optimised {
public:
	Optimised(Graph& gr);
	double opt_way();


private:
	vector<vector<double>> matrix;
};