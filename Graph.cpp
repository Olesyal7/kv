#include "Graph.h"
#include <vector>

using namespace std;

const double inf = 100500;

Graph::Graph(int size) {
	ver = size;
	matrix.resize(ver, vector<double>(ver, 0));
}

void Graph::add_edge(int from, int to, double weight) {
	if (from == to) {
		return;
	}
	matrix[from][to] = weight;
}

const vector<vector<double>>& Graph::get_matrix() {
	return matrix;
}