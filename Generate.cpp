#include "Generate.h"
#include "Point.h"
#include <vector>
#include <random>
#include <cmath>
#include <ctime>

using namespace std;

vector<Point> Set(int amount) {
	srand(time(0));
	vector<Point> set(0);
	random_device rd;
	uniform_real_distribution<double> urd(-1, 1);
	for (int i = 0; i < amount; ++i) {
		double x = urd(rd);
		double y = urd(rd);
		double s = x*x + y*y;
		while (s == 0 || s > 1) {
			x = urd(rd);
			y = urd(rd);
			s = x*x + y*y;
		}
		double zx = x*(sqrt((-2)*log(s) / s));
		double zy = y*(sqrt((-2)*log(s) / s));
		set.emplace_back(zx, zy);
	}
	return move(set);
}