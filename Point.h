#pragma once
#include <cmath>

struct Point {
	Point();
	Point(double _x, double _y);
	double x;
	double y;
};

double dist(Point p1, Point p2);