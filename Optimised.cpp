#include "Optimised.h"
#include "Graph.h"
#include <algorithm>

const int Min = 100500;

Optimised::Optimised(Graph& gr) {
	matrix = gr.get_matrix();
}

double Optimised::opt_way() {
	vector<int> my;
	double curMin = Min;
	for (int i = 0; i < matrix.size(); i++) {
		my.push_back(i);
	}
	do {
		double s = 0;
		for (int i = 1; i < matrix.size(); i++) {
			s += matrix[my[i]][my[i - 1]];
		}
		s += matrix[my[0]][my[matrix.size() - 1]];
		curMin = min(curMin, s);
	} while (next_permutation(my.begin(), my.end()));

	return curMin;
}

