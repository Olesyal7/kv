#include "DSU.h"

void DSU::buffer(int size) {
	root.resize(size, 0);
	rank.resize(size, 0);
	for (int i = 0; i < size; ++i) {
		root[i] = i;
	}
}

int DSU::get_set(int ex) {
	if (root[ex] != ex) {
		root[ex] = get_set(root[ex]);
	}
	return root[ex];
}

void DSU::unions(int ex1, int ex2) {
	ex1 = get_set(ex1);
	ex2 = get_set(ex2);
	if (ex1 == ex2) {
		return;
	}
	else {
		if (rank[ex1] == rank[ex2]) {
			rank[ex2]++;
		}
		if (rank[ex1] < rank[ex2]) {
			root[ex1] = ex2;
		}
		else {
			root[ex2] = ex1;
		}
		return;
	}
}