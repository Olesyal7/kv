#include <vector>
#include <iostream>
#include <algorithm>
#include <random>
#include "Point.h"
#include "DSU.h"
#include "Graph.h"
#include "MST.h"
#include "Generate.h"
#include "Optimised.h"

using namespace std;

void testing(int l, int r, int t) {
	for (int i = l; i <= r; i++) {
		double dif = 0;
		vector<double> difs(t);
		for (int j = 0; j < t; j++) {
			Graph gr(i);
			vector<Point> points = Set(i);
			for (int u = 0; u < i; ++u) {
				for (int v = 0; v < i; ++v) {
					gr.add_edge(u, v, dist(points[u], points[v]));
				}
			}
			MST mst(gr);
			Optimised opt(gr);
			mst.build();
			difs[j] = mst.aprox() / opt.opt_way();
			dif += difs[j];
		}
		dif /= t;
		double s = 0;
		for (int k = 0; k < t; k++) {
			s += (difs[k] - dif)*(difs[k] - dif);
		}
		if (t > 1) {
			s /= t - 1;
		}
		s = sqrt(s);
		cout << i << "  " << dif << "  " << s << endl;
	}
}

int main() {
	int l;
	int r;
	int t;
	cin >> l >> r >> t;
	testing(l, r, t);
	Graph gr(4);
	return 0;
}