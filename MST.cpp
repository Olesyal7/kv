#include "MST.h"
#include "Graph.h"
#include "Edge.h"
#include <algorithm>

MST::MST(Graph& graph) {
	matrix = graph.get_matrix();
	order.resize(matrix.size(), 0);
	color.resize(matrix.size(), 0);
}

const vector<vector<double>>& MST::get_matrix() {
	return matrix;
}

double MST::aprox() {
	double sum = 0;
	for (int i = 1; i < Tree.size(); i++) {
		sum += matrix[order[i]][order[i - 1]];
	}
	sum += matrix[order[0]][order[order.size() - 1]];
	return sum;
}

void MST::DFS(int i, int& count) {
	color[i] = 1;
	order[i] = count;
	count++;
	for (int j = 0; j < Tree.size(); j++) {
		if (color[j] == 0 && Tree[i][j] != 0) {
			DFS(j, count);
		}
	}
}

void MST::build() {
	int ver = matrix.size();
	Tree.resize(ver, vector<double>(ver, 0));
	vector<Edge> edges;
	for (int i = 0; i < ver; i++) {
		for (int j = 0; j < ver; j++) {
			if (i != j) {
				edges.push_back(Edge(i, j, matrix[i][j]));
			}
		}
	}
	sort(edges.begin(), edges.end(), [](Edge e1, Edge e2) {return e1.weight < e2.weight; });
	DSU trees;
	trees.buffer(ver);
	for (int i = 0; i < edges.size(); i++) {
		if (trees.get_set(edges[i].from) != trees.get_set(edges[i].to)) {
			trees.unions(edges[i].from, edges[i].to);
			Tree[edges[i].from][edges[i].to] = edges[i].weight;
			Tree[edges[i].to][edges[i].from] = edges[i].weight;
		}
	}
	int count = 0;
	DFS(0, count);
}