#include "Point.h"
#include <cmath>

using namespace std;

Point::Point(double _x, double _y) {
	x = _x;
	y = _y;
}

Point::Point() {
	x = 0;
	y = 0;
}

double dist(Point p1, Point p2) {
	return sqrt((p1.x - p2.x)*(p1.x - p2.x) + (p1.y - p2.y)*(p1.y - p2.y));
}